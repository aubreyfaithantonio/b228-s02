// console.log('hello')

//without the use of objects, our students from before would be organized as follows if we are to record additional information about them

//Spaghetti Code - when codes is not organized enough that it becomes hard to work on it

// //create student one
// let studentOneName = 'John';
// let studentOneEmail = 'john@mail.com';
// let studentOneGrades = [89, 84, 78, 88];

// //create student two
// let studentTwoName = 'Joe';
// let studentTwoEmail = 'joe@mail.com';
// let studentTwoGrades = [78, 82, 79, 85];

// //create student three
// let studentThreeName = 'Jane';
// let studentThreeEmail = 'jane@mail.com';
// let studentThreeGrades = [87, 89, 91, 93];

// //create student four
// let studentFourName = 'Jessie';
// let studentFourEmail = 'jessie@mail.com';
// let studentFourGrades = [91, 89, 92, 93];

// //actions that students may perform will be lumped together
// function login(email){
//     console.log(`${email} has logged in`);
// }

// function logout(email){
//     console.log(`${email} has logged out`);
// }

// function listGrades(grades){
//     grades.forEach(grade => {
//         console.log(grade);
//     })
// }

//This way of organizing employees is not well organized at all.
//This will become unmanageable when we add more employees or functions
//To remedy this, we will create objects

// let studentOne = {
// 	name: "John",1
// 	email: "john@mail.com",
// 	grades: [89, 84, 78, 88],

	//add functionalities
		//keyword "this" refers to the object encapsulating the method where "this" is called

// 	login(){
// 		console.log(`${this.name} has logged in`)
// 	},

// 	logout(){
// 		console.log(`${this.name} has logged out`)
// 	},

// 	listGrades(){
// 		console.log(`Student one's quarterly averages are 
// 			${this.grades}`)
// 	}
// }

// console.log(`Student one's name is ${studentOne.name}`)
// console.log(`Student one's email is ${studentOne.email}`)
// console.log(`Student one's grade averages are ${studentOne.grades}`)

// console.clear()

// //create student two
// let studentTwoName = 'Joe';
// let studentTwoEmail = 'joe@mail.com';
// let studentTwoGrades = [78, 82, 79, 85];

// let studentTwo = {
// 	name: "Joe",
// 	email: "joe@mail.com",
// 	grades: [78, 82, 79, 85],

// 	login(){
// 		console.log(`${this.name} has logged in`)
// 	},

// 	logout(){
// 		console.log(`${this.name} has logged out`)
// 	},

// 	listGrades(){
// 		console.log(`Student two's quarterly averages are 
// 			${this.grades}`)
// 	}

// 	// avgGrade(x){
// 	//     let sum = x.reduce((a, b) => a + b)
// 	//     let avg = sum / x.length
// 	//     console.log(sum)
// 	//     console.log(avg)
// 	// }
// }

// console.log(`Student two's name is ${studentTwo.name}`)
// console.log(`Student two's email is ${studentTwo.email}`)
// console.log(`Student two's grade averages are ${studentTwo.grades}`)
// console.log(studentTwo.login())
// console.log(studentTwo.logout())
// console.log(studentTwo.listGrades())

// console.clear()

// ----------------------------------------------------------
//                        ACTIVITY
// ----------------------------------------------------------

// PART 1 QUIZ

// 1. What is the term given to unorganized code that's very hard to work with?
// Spaghetti Code

// 2. How are object literals written in JS?
// {} - curly brackets 

/*
	ex. 
	let car = {
		color: "red"
	}
*/

// 3. What do you call the concept of organizing information and functionality to belong to an object?
// Encapsulation

// 4. If the studentOne object has a method named enroll(), how would you invoke it?
// studentOne.enroll()

// 5. True or False: Objects can have objects as properties.
// True

// 6. What is the syntax in creating key-value pairs?
// key : "value"

/*

	ex.
		let dog = {
			breed: "Siberian Husky"
		}

*/

// 7. True or False: A method can have no parameters and still work.
// True

// 8. True or False: Arrays can have objects as elements.
// True

// 9. True or False: Arrays are objects.
// True

// 10. True or False: Objects can have arrays as properties.
// True

// PART 2 CODING

// 1. Translate the other students from our boilerplate code into their own respective objects.

		// let studentOne = {
		// 	name: "John",
		// 	email: "john@mail.com",
		// 	grades: [89, 84, 78, 88]
		// }

		// let studentTwo = {
		// 	name: "Joe",
		// 	email: "joe@mail.com",
		// 	grades: [78, 82, 79, 85]
		// }

		// let studentThree = {
		// 	name: "Jane",
		// 	email: "jane@mail.com",
		// 	grades: [87, 89, 91, 93]
		// }

		// let studentFour = {
		// 	name: "Jessie",
		// 	email: "jessie@mail.com",
		// 	grades: [91, 89, 92, 93]
		// }

// 2. Define a method for EACH student object that will compute for their grade average (total of grades divided by 4)

		// let studentOne = {
		// 	name: "John",
		// 	email: "john@mail.com",
		// 	grades: [89, 84, 78, 88],

		// 	getAverage(){
		// 		let sum = this.grades.reduce((a,b) => a + b)
		// 		let ave = sum / this.grades.length

		// 		return ave
		// 	}
		// }

		// let studentTwo = {
		// 	name: "Joe",
		// 	email: "joe@mail.com",
		// 	grades: [78, 82, 79, 85],

		// 	getAverage(){
		// 		let sum = this.grades.reduce((a,b) => a + b)
		// 		let ave = sum / this.grades.length

		// 		return ave
		// 	}
		// }

		// let studentThree = {
		// 	name: "Jane",
		// 	email: "jane@mail.com",
		// 	grades: [87, 89, 91, 93],

		// 	getAverage(){
		// 		let sum = this.grades.reduce((a,b) => a + b)
		// 		let ave = sum / this.grades.length

		// 		return ave
		// 	}
		// }

		// let studentFour = {
		// 	name: "Jessie",
		// 	email: "jessie@mail.com",
		// 	grades: [91, 89, 92, 93],

		// 	getAverage(){
		// 		let sum = this.grades.reduce((a,b) => a + b)
		// 		let ave = sum / this.grades.length

		// 		return ave
		// 	}
		// }

		// console.log(studentOne.getAverage())
		// console.log(studentTwo.getAverage())
		// console.log(studentThree.getAverage())
		// console.log(studentFour.getAverage())


// 3. Define a method for all student objects named willPass() that returns a Boolean value indicating if student will pass or fail. For a student to pass, their ave. grade must be greater than or equal to 85.

		// let studentOne = {
		// 	name: "John",
		// 	email: "john@mail.com",
		// 	grades: [89, 84, 78, 88],

		// 	getAverage(){
		// 		let sum = this.grades.reduce((a,b) => a + b)
		// 		let ave = sum / this.grades.length

		// 		return ave
		// 	},

		// 	willPass(){
		// 		let ave = this.getAverage()
		// 		return (ave >= 85) ? true : false
		// 	}
		// }

		// let studentTwo = {
		// 	name: "Joe",
		// 	email: "joe@mail.com",
		// 	grades: [78, 82, 79, 85],

		// 	getAverage(){
		// 		let sum = this.grades.reduce((a,b) => a + b)
		// 		let ave = sum / this.grades.length

		// 		return ave
		// 	},

		// 	willPass(){
		// 		let ave = this.getAverage()
		// 		return (ave >= 85) ? true : false
		// 	}
		// }

		// let studentThree = {
		// 	name: "Jane",
		// 	email: "jane@mail.com",
		// 	grades: [87, 89, 91, 93],

		// 	getAverage(){
		// 		let sum = this.grades.reduce((a,b) => a + b)
		// 		let ave = sum / this.grades.length

		// 		return ave
		// 	},

		// 	willPass(){
		// 		let ave = this.getAverage()
		// 		return (ave >= 85) ? true : false
		// 	}
		// }

		// let studentFour = {
		// 	name: "Jessie",
		// 	email: "jessie@mail.com",
		// 	grades: [91, 89, 92, 93],

		// 	getAverage(){
		// 		let sum = this.grades.reduce((a,b) => a + b)
		// 		let ave = sum / this.grades.length

		// 		return ave
		// 	},

		// 	willPass(){
		// 		let ave = this.getAverage()
		// 		return (ave >= 85) ? true : false
		// 	}
		// }

		// console.log(studentOne.willPass())
		// console.log(studentTwo.willPass())
		// console.log(studentThree.willPass())
		// console.log(studentFour.willPass())

// 4. Define a method for all student objects named willPassWithHonors() that returns true if ave. grade is greater than or equal to 90, false if >= 85 but < 90, and undefined if < 85 (since student will not pass).

		// let studentOne = {
		// 	name: "John",
		// 	email: "john@mail.com",
		// 	grades: [89, 84, 78, 88],

		// 	getAverage(){
		// 		let sum = this.grades.reduce((a,b) => a + b)
		// 		let ave = sum / this.grades.length

		// 		return ave
		// 	},

		// 	willPass(){
		// 		let ave = this.getAverage()
		// 		return (ave >= 85) ? true : false
		// 	},

		// 	willPassWithHonors(){
		// 		let ave = this.getAverage()
		// 		if(ave >= 90) return true
		// 		if(ave >= 85 && ave < 90) return false
		// 		if(ave < 85) return undefined
		// 	}
		// }

		// let studentTwo = {
		// 	name: "Joe",
		// 	email: "joe@mail.com",
		// 	grades: [78, 82, 79, 85],

		// 	getAverage(){
		// 		let sum = this.grades.reduce((a,b) => a + b)
		// 		let ave = sum / this.grades.length

		// 		return ave
		// 	},

		// 	willPass(){
		// 		let ave = this.getAverage()
		// 		return (ave >= 85) ? true : false
		// 	},

		// 	willPassWithHonors(){
		// 		let ave = this.getAverage()
		// 		if(ave >= 90) return true
		// 		if(ave >= 85 && ave < 90) return false
		// 		if(ave < 85) return undefined
		// 	}
		// }

		// let studentThree = {
		// 	name: "Jane",
		// 	email: "jane@mail.com",
		// 	grades: [87, 89, 91, 93],

		// 	getAverage(){
		// 		let sum = this.grades.reduce((a,b) => a + b)
		// 		let ave = sum / this.grades.length

		// 		return ave
		// 	},

		// 	willPass(){
		// 		let ave = this.getAverage()
		// 		return (ave >= 85) ? true : false
		// 	},

		// 	willPassWithHonors(){
		// 		let ave = this.getAverage()
		// 		if(ave >= 90) return true
		// 		if(ave >= 85 && ave < 90) return false
		// 		if(ave < 85) return undefined
		// 	}
		// }

		// let studentFour = {
		// 	name: "Jessie",
		// 	email: "jessie@mail.com",
		// 	grades: [91, 89, 92, 93],

		// 	getAverage(){
		// 		let sum = this.grades.reduce((a,b) => a + b)
		// 		let ave = sum / this.grades.length

		// 		return ave
		// 	},

		// 	willPass(){
		// 		let ave = this.getAverage()
		// 		return (ave >= 85) ? true : false
		// 	},

		// 	willPassWithHonors(){
		// 		let ave = this.getAverage()
		// 		if(ave >= 90) return true
		// 		if(ave >= 85 && ave < 90) return false
		// 		if(ave < 85) return undefined
		// 	}
		// }

		// console.log(studentOne.willPassWithHonors())
		// console.log(studentTwo.willPassWithHonors())
		// console.log(studentThree.willPassWithHonors())
		// console.log(studentFour.willPassWithHonors())

// 5. Create an object named classOf1A with a property named students which is an array containing all 4 student objects in it.
	// let classOf1A = {
	// 	students: [
	// 		{
	// 			name: "John",
	// 			email: "john@mail.com",
	// 			grades: [89, 84, 78, 88]
	// 		},
	// 		{
	// 			name: "Joe",
	// 			email: "joe@mail.com",
	// 			grades: [78, 82, 79, 85]
	// 		},
	// 		{
	// 			name: "Jane",
	// 			email: "jane@mail.com",
	// 			grades: [87, 89, 91, 93]
	// 		},
	// 		{
	// 			name: "Jessie",
	// 			email: "jessie@mail.com",
	// 			grades: [91, 89, 92, 93]
	// 		}

	// 	]
	// }

	// console.log(classOf1A)

// 6. Create a method for the object classOf1A named countHonorStudents() that will return the number of honor students.
	// let classOf1A = {
	// 	students: [
	// 		{
	// 			name: "John",
	// 			email: "john@mail.com",
	// 			grades: [89, 84, 78, 88]
	// 		},
	// 		{
	// 			name: "Joe",
	// 			email: "joe@mail.com",
	// 			grades: [78, 82, 79, 85]
	// 		},
	// 		{
	// 			name: "Jane",
	// 			email: "jane@mail.com",
	// 			grades: [87, 89, 91, 93]
	// 		},
	// 		{
	// 			name: "Jessie",
	// 			email: "jessie@mail.com",
	// 			grades: [91, 89, 92, 93]
	// 		}
	// 	],

	// 	countHonorStudents(){
	// 		let honorStudents = []

	// 		this.students.forEach((data) => {
	// 			let sum = data.grades.reduce((a,b) => a + b)
	// 			let ave = sum / data.grades.length

	// 			if(ave >= 90) honorStudents.push(ave)
	// 		})

	// 		return honorStudents.length
	// 	}
	// }

	// console.log(classOf1A.countHonorStudents())

	
// 7. Create a method for the object classOf1A named honorsPercentage() that will return the % of honor students from the batch's total number of students.
	// let classOf1A = {
	// 	students: [
	// 		{
	// 			name: "John",
	// 			email: "john@mail.com",
	// 			grades: [89, 84, 78, 88]
	// 		},
	// 		{
	// 			name: "Joe",
	// 			email: "joe@mail.com",
	// 			grades: [78, 82, 79, 85]
	// 		},
	// 		{
	// 			name: "Jane",
	// 			email: "jane@mail.com",
	// 			grades: [87, 89, 91, 93]
	// 		},
	// 		{
	// 			name: "Jessie",
	// 			email: "jessie@mail.com",
	// 			grades: [91, 89, 92, 93]
	// 		}
	// 	],

	// 	countHonorStudents(){
	// 		let honorStudents = []

	// 		this.students.forEach((data) => {
	// 			let sum = data.grades.reduce((a,b) => a + b)
	// 			let ave = sum / data.grades.length

	// 			if(ave >= 90) honorStudents.push(ave)
	// 		})

	// 		return honorStudents.length
	// 	},

	// 	honorsPercentage(){
	// 		let honors = this.countHonorStudents()
	// 		let totalStudents = this.students.length

	// 		return (100 * honors) / totalStudents;
	// 	}
	// }

	// console.log(classOf1A.honorsPercentage())


// 8. Create a method for the object classOf1A named retrieveHonorStudentInfo() that will return all honor students' emails and ave. grades as an array of objects.
	// let classOf1A = {
	// 	students: [
	// 		{
	// 			name: "John",
	// 			email: "john@mail.com",
	// 			grades: [89, 84, 78, 88]
	// 		},
	// 		{
	// 			name: "Joe",
	// 			email: "joe@mail.com",
	// 			grades: [78, 82, 79, 85]
	// 		},
	// 		{
	// 			name: "Jane",
	// 			email: "jane@mail.com",
	// 			grades: [87, 89, 91, 93]
	// 		},
	// 		{
	// 			name: "Jessie",
	// 			email: "jessie@mail.com",
	// 			grades: [91, 89, 92, 93]
	// 		}
	// 	],

	// 	retrieveHonorStudentInfo() {
	// 		let honorStudents = []

	// 		this.students.forEach((data) => {
	// 			let sum = data.grades.reduce((a,b) => a + b)
	// 			let ave = sum / data.grades.length

	// 			if(ave >= 90) honorStudents.push({
	// 				aveGrade: ave,
	// 				email: data.email
	// 			})
	// 		})

	// 		return honorStudents
	// 	}
	// }

	// console.log(classOf1A.retrieveHonorStudentInfo())

// 9. Create a method for the object classOf1A named sortHonorStudentsByGradeDesc() that will return all honor students' emails and ave. grades as an array of objects sorted in descending order based on their grade averages.
	
	// let classOf1A = {
	// 	students: [
	// 		{
	// 			name: "John",
	// 			email: "john@mail.com",
	// 			grades: [89, 84, 78, 88]
	// 		},
	// 		{
	// 			name: "Joe",
	// 			email: "joe@mail.com",
	// 			grades: [78, 82, 79, 85]
	// 		},
	// 		{
	// 			name: "Jane",
	// 			email: "jane@mail.com",
	// 			grades: [87, 89, 91, 93]
	// 		},
	// 		{
	// 			name: "Jessie",
	// 			email: "jessie@mail.com",
	// 			grades: [91, 89, 92, 93]
	// 		}
	// 	],

	// 	retrieveHonorStudentInfo() {
	// 		let honorStudents = []

	// 		this.students.forEach((data) => {
	// 			let sum = data.grades.reduce((a,b) => a + b)
	// 			let ave = sum / data.grades.length

	// 			if(ave >= 90) honorStudents.push({
	// 				aveGrade: ave,
	// 				email: data.email
	// 			})
	// 		})

	// 		return honorStudents
	// 	},

	// 	sortHonorStudentsByGradeDesc(){
	// 		return this.retrieveHonorStudentInfo().sort((a, b) => {

	// 			if (a.aveGrade > b.aveGrade){
	// 				return -1;
	// 			}

	// 			if ( a.aveGrade < b.aveGrade ){
	// 			    return 1;
	// 			}

	// 			return 0;
	// 		})
	// 	}
	// }

	// console.log(classOf1A.sortHonorStudentsByGradeDesc())